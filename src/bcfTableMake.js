var aux_filename = '/home/michael/Dropbox/temp/maus_aux.xls',
  raw_filename = '/home/michael/Dropbox/temp/maus_raw.xls',
  dates = [2018.4, 2020.2];

var xlsx = require('node-xlsx');
var { estimate, transpose } = require('./bcfEstimate.js');
var { excelParseRaw, excelParseAux } = require('./excelParse.js');

var tableRows = [
  [
    'Effective Federal Funds Rate (RFF)',
    '',
    '',
    'Prime Lending Interest Rate (%) (RPRM)',
    '',
    '',
    'LIBOR 3-Month Rate',
    '',
    '',
    'Commercial Paper (Financial) - 1 Mo.',
    '',
    '',
    'Constant Maturity Treasury Yield, 3 Mos (%) (RT3M)',
    '',
    '',
    'Constant Maturity Treasury Yield, 6 Mos (%) (RT6M)',
    '',
    '',
    'Constant Maturity Treasury Yield, 1 Yr (%) (RT1Y)',
    '',
    '',
  ],
  [
    'Constant Maturity Treasury Yield, 2 Yrs (%) (RT2Y)',
    '',
    '',
    'Constant Maturity Treasury Yield, 5 Yrs (%) (RT5Y)',
    '',
    '',
    'Constant Maturity Treasury Yield, 10 Yrs (RT10Y)',
    '',
    '',
    'Constant Maturity Treasury Yield, 30 Yrs  (RT30Y)',
    '',
    '',
    'Corporate Aaa Bonds (Yield)',
    '',
    '',
    'BAA Corporate Bond Yield (%) (RCB)',
    '',
    '',
    'State & Local Bonds (Yield)',
    '',
    '',
  ],
  [
    'FHA Secondary-Market Mortgage Yield (RMTG)',
    '',
    '',
    "FRB's Major Currency Index",
    '',
    '',
    'Gross Domestic Product (bil chained 09 $) (GDP)',
    '',
    '',
    'GDP Price Index (Q/Q SAAR) (PGDP) (PGDP2012)',
    '',
    '',
    'Consumer Price Index (1982-1984=100) (PCPI)',
    '',
    '',
  ],
];

var raw0 = excelParseRaw(raw_filename, dates);

if (raw0.status !== 1) {
  // return raw0;
  console.error(raw0);
}

var aux = excelParseAux(aux_filename, dates);

if (aux.status !== 1) {
  // return aux;
  console.error(aux);
}

var raw1 = estimate(aux.data, aux.rows, aux.cols);

var raw = Object.assign(raw0.data, raw1.data);

var out = [],
  ec;
for (let ii = 0; ii < tableRows.length; ii++) {
  let tr = tableRows[ii];

  let rr = transpose(
    [[`Row ${ii + 1}`].concat(aux.cols)].concat(
      tr.map((tri) => [tri].concat(raw[tri] || '')),
    ),
  );

  if (!ec) {
    ec = rr[0].map((jj) => '');
  }

  out = out.concat(rr);
  out.push(ec);
  out.push(ec);
}
