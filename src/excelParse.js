var xlsx = require('node-xlsx');
var XLSX = require('xlsx');

// var filename = '/home/michael/Dropbox/temp/maus_aux.xls',
//   dates = [2018.4, 2020.2];

const excelParseOnline = (af, callback) => {
  const reader = new FileReader();
  reader.onload = (e) => {
    const workSheet = XLSX.read(e.target.result, { type: 'binary' });
    const wb = Object.keys(workSheet.Sheets).map((name) => {
      const sheet = workSheet.Sheets[name];
      return {
        name,
        data: XLSX.utils.sheet_to_json(sheet, {
          header: 1,
          raw: true,
        }),
      };
    });
    callback(wb[0].data);
  };
  reader.readAsBinaryString(af);
};

const excelParseAux = (filename, obj, dates) => {
  if (!obj) {
    obj = xlsx.parse(filename)[0].data; // parses a file
  }
  var d = obj.filter((r) => r.length > 0);

  var cols = d[0].filter((c) => c && c >= dates[0] && c <= dates[1]);
  if (cols.length === 0) {
    return {
      status: -1,
      msg: `Could not find dates in the data that matched dates ${dates}`,
    };
  }

  var bds = [d[0].indexOf(cols[0]), d[0].indexOf(cols.slice(-1)[0]) + 1];
  cols = cols.map((c) => c.toString());
  var rows = d.map((r) => r[0]).filter((r) => r);
  var data = d.slice(1).map((r) => r.slice(bds[0], bds[1]));

  return { status: 1, data, rows, cols };
};

// var filename = '/home/michael/Dropbox/temp/maus_raw.xls',
//   dates = [2018.4, 2020.2];

const excelParseRaw = (filename, obj, dates) => {
  const rows = [
    'Effective Federal Funds Rate (RFF)',
    'Prime Lending Interest Rate (%) (RPRM)',
    'Constant Maturity Treasury Yield, 3 Mos (%) (RT3M)',
    'Constant Maturity Treasury Yield, 6 Mos (%) (RT6M)',
    'Constant Maturity Treasury Yield, 1 Yr (%) (RT1Y)',
    'Constant Maturity Treasury Yield, 2 Yrs (%) (RT2Y)',
    'Constant Maturity Treasury Yield, 5 Yrs (%) (RT5Y)',
    'Constant Maturity Treasury Yield, 10 Yrs (RT10Y)',
    'Constant Maturity Treasury Yield, 30 Yrs  (RT30Y)',
    'BAA Corporate Bond Yield (%) (RCB)',
    'FHA Secondary-Market Mortgage Yield (RMTG)',
    'Gross Domestic Product (bil chained 09 $) (GDP)',
    'GDP Price Index (Q/Q SAAR) (PGDP) (PGDP2012)',
    'Consumer Price Index (1982-1984=100) (PCPI)',
  ];

  if (!obj) {
    var obj = xlsx.parse(filename)[0].data; // parses a file
  }

  var d = obj.filter((r) => r.length > 0);

  var cols = d[2];
  if (!cols.includes(dates[0]) || !cols.includes(dates[1])) {
    return {
      status: -1,
      msg: `The columns ${cols.slice(
        1,
      )} did not include the specified dates (${dates})`,
    };
  }

  var bds = [cols.indexOf(dates[0]), cols.indexOf(dates[1]) + 1];
  cols = cols.slice(bds[0], bds[1]);

  var data = {};
  for (let ii = 0; ii < rows.length; ii++) {
    let r = rows[ii],
      found = false;
    for (let jj = 0; jj < d.length; jj++) {
      if (typeof d[jj][0] === 'string' && d[jj][0].startsWith(r)) {
        data[r] = d[jj].slice(bds[0], bds[1]);
        found = true;
        break;
      }
    }

    if (!found) {
      return {
        status: -1,
        msg: `Could not find entries for ${r}`,
      };
    }
  }

  return { status: 1, data, cols };
};

const excelParseRawOnline = (obj, dates, callback) => {
  const inner = (wb) => {
    callback(excelParseRaw(null, wb, dates));
  };

  excelParseOnline(obj, inner);
};

const excelParseAuxOnline = (obj, dates, callback) => {
  const inner = (wb) => {
    callback(excelParseAux(null, wb, dates));
  };

  excelParseOnline(obj, inner);
};

module.exports = {
  excelParseRaw,
  excelParseAux,
  excelParseRawOnline,
  excelParseAuxOnline,
};
