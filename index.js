import React from 'react';
import ReactDOM from 'react-dom';

import 'semantic-ui-css/semantic.min.css';
import { Tab, Segment } from 'semantic-ui-react';

import './style.css';
import SitecoreHTML from './src/SitecoreHTML.js';
import BlueChipFinancials from './src/BlueChipFinancials.js';

const panes = [
  {
    menuItem: 'Sitecore HTML',
    render: () => (
      <Tab.Pane>
        <SitecoreHTML />
      </Tab.Pane>
    ),
  },
  {
    menuItem: 'Blue Chip Financials',
    render: () => (
      <Tab.Pane as={Segment}>
        <BlueChipFinancials />
      </Tab.Pane>
    ),
  },
  {
    menuItem: 'Blue Chip Economics',
    render: () => <Tab.Pane>Tab 3 Content</Tab.Pane>,
  },
  {
    menuItem: 'Back Page Table',
    render: () => <Tab.Pane>Tab 3 Content</Tab.Pane>,
  },
];

class App extends React.Component {
  render() {
    return (
      <div>
        <Tab
          activeIndex={1}
          menu={{ secondary: true, pointing: true }}
          panes={panes}
        />
      </div>
    );
  }
}

var mountNode = document.getElementById('app');
ReactDOM.render(<App />, mountNode);
