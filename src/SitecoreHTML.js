import React from 'react';
import ReactDOM from 'react-dom';

import {
  Form,
  TextArea,
  Button,
  Checkbox,
  Select,
  Input,
  Divider,
  Segment,
  Card,
  Tab,
  Header,
  Message,
  Icon,
} from 'semantic-ui-react';

import {
  economicBearingsTemplate,
  economicCurrentsTemplate,
} from './sitecoreTemplates.js';

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const MONTH_OPTIONS = MONTHS.map((m) => {
  return { key: m, value: m, text: m };
});

const PUBLICATION_OPTIONS = [
  { key: 0, value: 0, text: 'Economic Bearings' },
  { key: 1, value: 1, text: 'Economic Currents' },
];

const BEARINGS_OPTIONS = [
  {
    key: 'employment-situation',
    value: 'employment-situation',
    text: 'Employment Situation',
  },
  {
    key: 'employment-primer',
    value: 'employment-primer',
    text: 'Employment Primer',
  },
  {
    key: 'fed-primer',
    value: 'fed-primer',
    text: 'Fed Primer',
  },
  {
    key: 'fomc',
    value: 'fomc',
    text: 'FOMC (Federal Open Market Committee)',
  },
  {
    key: 'retail-sales',
    value: 'retail-sales',
    text: 'Retail Sales',
  },

  {
    key: 'housing-starts',
    value: 'housing-starts',
    text: 'Housing Starts',
  },
  {
    key: 'new-home-sales',
    value: 'new-home-sales',
    text: 'New Home Sales',
  },
  {
    key: 'durable-goods',
    value: 'durable-goods',
    text: 'Durable Goods',
  },
  {
    key: 'pce',
    value: 'pce',
    text: 'PCE (Personal Consumption Expenditures)',
  },
  {
    key: 'construction',
    value: 'construction',
    text: 'Construction',
  },
  {
    key: 'cpi',
    value: 'cpi',
    text: 'CPI (Consumer Price Index)',
  },
  {
    key: 'gdp',
    value: 'gdp',
    text: 'GDP (Gross Domestic Product)',
  },
  {
    key: 'ppi',
    value: 'ppi',
    text: 'PPI (Producer Price Index)',
  },
];

const InputTab = ({
  title,
  subtitle,
  body,
  templatedText,
  bodyIsRaw,
  publicationType,
  currentsImage,
  currentsPdf,
  currentsMonth,
  bearingsRelease,
  bearingsDate,
  onSetState,
  onSubmit,
  errorState,
}) => (
  <Tab.Pane>
    <Message error hidden={!errorState}>
      <Message.Header>
        <Icon name="warning circle" /> Fill in all required fields
      </Message.Header>
    </Message>
    <Form>
      <Form.Field>
        <label>Publication Type</label>
        <Select
          placeholder="Publication Type"
          options={PUBLICATION_OPTIONS}
          value={publicationType}
          onChange={() =>
            onSetState({
              publicationType: 1 * !publicationType,
            })
          }
        />
      </Form.Field>
      <Form.Field>
        <label className={`required ${errorState ? 'error' : ''}`}>Title</label>
        <Input
          placeholder="Title"
          value={title || ''}
          onChange={(e, d) => onSetState({ title: d.value })}
        />
      </Form.Field>
      <Form.Field>
        <label>Subtitle</label>
        <Input
          type="text"
          placeholder="Subtitle"
          value={subtitle || ''}
          onChange={(e, d) => onSetState({ subtitle: d.value })}
        />
      </Form.Field>
      <Form.Field>
        <label className={`required ${errorState ? 'error' : ''}`}>
          Body of Text
        </label>
        <TextArea
          rows={10}
          placeholder="Body of text"
          value={body || ''}
          onChange={(e, d) => onSetState({ body: d.value })}
        />
      </Form.Field>
      <Form.Field>
        <Checkbox
          label="Use body text as raw input."
          checked={bodyIsRaw}
          onChange={(e, d) => onSetState({ bodyIsRaw: !bodyIsRaw })}
        />
      </Form.Field>
      {publicationType ? (
        <Card fluid>
          <Card.Content>
            <Card.Header>Economic Currents Options</Card.Header>
            <br />
            <Form.Field>
              <label>Publication Month</label>
              <Select
                placeholder="Publication Type"
                options={MONTH_OPTIONS}
                value={currentsMonth}
                onChange={(e, d) =>
                  onSetState({
                    currentsMonth: d.value,
                  })
                }
              />
            </Form.Field>
            <Form.Field>
              <label>Image URL</label>
              <Input
                type="text"
                placeholder="Should look like '-/media/4A0D892EA3034BBCB70B73907FD6A97C.ashx'"
                value={currentsImage || ''}
                onChange={(e, d) => onSetState({ currentsImage: d.value })}
              />
            </Form.Field>
            <Form.Field>
              <label>PDF URL</label>
              <Input
                type="text"
                placeholder="Should look like '-/media/8EB8FA1E74004DCFB0F702FF9E47AEFB.ashx'"
                value={currentsPdf || ''}
                onChange={(e, d) => onSetState({ currentsPdf: d.value })}
              />
            </Form.Field>
          </Card.Content>
        </Card>
      ) : (
        <Card fluid>
          <Card.Content>
            <Card.Header>Economic Bearings Options</Card.Header>
            <br />
            <Form.Field>
              <label className={`required ${errorState ? 'error' : ''}`}>
                Data Release
              </label>
              <Select
                placeholder="Data Release"
                options={BEARINGS_OPTIONS}
                value={bearingsRelease}
                onChange={(e, d) =>
                  onSetState({
                    bearingsRelease: d.value,
                  })
                }
                error={errorState}
                required
              />
            </Form.Field>
            <Form.Field>
              <label>Publication Date</label>
              <Input
                type="date"
                placeholder="Publication Date"
                value={bearingsDate}
                onChange={(e, d) => onSetState({ bearingsDate: d.value })}
              />
            </Form.Field>
          </Card.Content>
        </Card>
      )}
      <Button type="submit" onClick={onSubmit}>
        Get HTML
      </Button>
    </Form>
  </Tab.Pane>
);

const OutputTab = ({ hasChanged, templatedText, publicationType, docName }) => (
  <Tab.Pane id="sitecore-html-output">
    <Header as="h3">{docName}</Header>
    <Header as="h5">
      {publicationType ? 'Economic Currents' : 'Economic Bearings'}
    </Header>
    <p>Copy and paste the text below into the HTML editor in Sitecore.</p>
    <Message warning hidden={!hasChanged}>
      <Message.Header>
        <Icon name="warning sign" />
        The inputs have been changed!
      </Message.Header>
      To get the most recent HTML go back to the <code>Input</code> tab and
      re-submit.
    </Message>
    <Form>
      <TextArea rows={35} value={templatedText} />
    </Form>
  </Tab.Pane>
);

export default class SitecoreHTML extends React.Component {
  constructor(props) {
    super(props);

    var d = new Date();
    var di = `${d.getFullYear()}-${(d.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${d.getDate()}`;

    this.state = {
      title: null,
      subtitle: null,
      body: null,
      templatedText: null,
      bodyIsRaw: false,
      publicationType: 0,
      currentsImage: null,
      currentsPdf: null,
      currentsMonth: MONTHS[new Date().getMonth()],
      bearingsRelease: null,
      bearingsDate: di,
      docName: null,
      hasChanged: false,
      activeIndex: 0,
      inputStateError: false,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  onUpdate(v) {
    v.hasChanged = true;
    this.setState(v);
  }

  onSubmit() {
    if (!this.state.title) {
      this.setState({ inputStateError: true });
      return;
    } else if (!this.state.body) {
      this.setState({ inputStateError: true });
      return;
    } else if (!this.state.publicationType && !this.state.bearingsRelease) {
      this.setState({ inputStateError: true });
      return;
    }

    if (this.state.publicationType) {
      this.setState({
        templatedText: economicCurrentsTemplate(
          this.state.title,
          this.state.subtitle,
          this.state.body,
          this.state.bodyIsRaw,
          this.state.currentsImage,
          this.state.currentsPdf,
          this.state.currentsMonth,
        ),
        docName: `economic-outlook-${this.state.currentsMonth.toLowerCase()}-${new Date().getFullYear()}`,
        hasChanged: false,
        activeIndex: 1,
        inputStateError: false,
      });
    } else {
      var d = this.state.bearingsDate;
      var docName = `${this.state.bearingsRelease}-${d.slice(5, 7)}${d.slice(
        8,
        10,
      )}${d.slice(2, 4)}`;
      this.setState({
        templatedText: economicBearingsTemplate(
          this.state.title,
          this.state.subtitle,
          this.state.body,
          this.state.bodyIsRaw,
        ),
        docName: docName,
        hasChanged: false,
        activeIndex: 1,
        inputStateError: false,
      });
    }
  }

  render() {
    var panes = [
      {
        menuItem: 'Input',
        render: () => (
          <InputTab
            title={this.state.title}
            subtitle={this.state.subtitle}
            body={this.state.body}
            templatedText={this.state.templatedText}
            bodyIsRaw={this.state.bodyIsRaw}
            publicationType={this.state.publicationType}
            currentsImage={this.state.currentsImage}
            currentsPdf={this.state.currentsPdf}
            currentsMonth={this.state.currentsMonth}
            bearingsRelease={this.state.bearingsRelease}
            bearingsDate={this.state.bearingsDate}
            errorState={this.state.inputStateError}
            onSetState={this.onUpdate}
            onSubmit={this.onSubmit}
          />
        ),
      },
    ];
    if (this.state.templatedText) {
      panes.push({
        menuItem: 'Output: HTML',
        render: () => (
          <OutputTab
            templatedText={this.state.templatedText}
            publicationType={this.state.publicationType}
            hasChanged={this.state.hasChanged}
            docName={this.state.docName}
          />
        ),
      });
    }

    const menuOpts = { fluid: true, vertical: true, tabular: false };

    return (
      <Tab
        onTabChange={(e, d) => this.setState({ activeIndex: d.activeIndex })}
        activeIndex={this.state.activeIndex}
        menu={menuOpts}
        id="sitecore-html"
        panes={panes}
      />
    );
  }
}
