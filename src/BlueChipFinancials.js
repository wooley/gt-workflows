import React from 'react';
import ReactDOM from 'react-dom';

import {
  Form,
  TextArea,
  Button,
  Checkbox,
  Select,
  Input,
  Divider,
  Segment,
  Card,
  Tab,
  Header,
  Message,
  Icon,
} from 'semantic-ui-react';
import Dropzone from 'react-dropzone';
import classNames from 'classnames';

import { excelParseRawOnline, excelParseAuxOnline } from './excelParse.js';
var xlsx = require('node-xlsx');

const QUARTER_OPTIONS = [1, 2, 3, 4].map((q) => {
  return {
    key: q,
    value: q,
    text: `Q${q}`,
  };
});

class FileDropzone extends React.Component {
  constructor(props) {
    super(props);

    this.onDrop = this.onDrop.bind(this);
  }

  onDrop(acceptedFiles, rejectedFiles) {
    if (acceptedFiles.length > 0) {
      this.props.onDrop(acceptedFiles[0]);
    }
  }

  render() {
    return (
      <Dropzone onDrop={this.onDrop}>
        {({ getRootProps, getInputProps, isDragActive }) => {
          return (
            <div
              {...getRootProps()}
              className={classNames('dropzone', {
                'dropzone--isActive': isDragActive,
              })}
            >
              <Input {...getInputProps()} />
              {isDragActive ? (
                <p>Drop files here...</p>
              ) : (
                <p>Drop the file here or click to select files to upload.</p>
              )}
            </div>
          );
        }}
      </Dropzone>
    );
  }
}

export default class BlueChipFinancials extends React.Component {
  constructor(props) {
    super(props);

    var d = new Date();
    var di = `${d.getFullYear()}-${(d.getMonth() + 1)
      .toString()
      .padStart(2, '0')}-${d.getDate()}`;

    this.state = {
      aux_file: null,
      raw_file: null,
      dates: [2018.3, 2019.3],
      dates2: [{ year: 2018, quarter: 3 }, { year: 2019, quarter: 3 }],
      data: null,
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
    this.onDropRaw = this.onDropRaw.bind(this);
    this.onDropAux = this.onDropAux.bind(this);
    this.onDateUpdate = this.onDateUpdate.bind(this);
  }

  onUpdate(v) {
    null;
  }

  onSubmit() {
    console.log('would submit');
  }

  onDropRaw(d) {
    excelParseRawOnline(d, this.state.dates, (raw_file) =>
      this.setState({ raw_file }),
    );
  }

  onDropAux(d) {
    excelParseAuxOnline(d, this.state.dates, (aux_file) =>
      this.setState({ aux_file }),
    );
  }

  onDateUpdate(v, kind, idx) {
    var d2 = this.state.dates2;
    var d = this.state.dates;
    d2[idx][kind] = parseInt(v);

    d[0] = parseFloat(`${d2[0].year}.${d2[0].quarter}`);
    d[1] = parseFloat(`${d2[1].year}.${d2[1].quarter}`);

    console.log(d2, d);

    this.setState({ dates2: d2, dates: d });
  }

  render() {
    return (
      <div id="blue-chip-financials">
        <Form as={Card} fluid>
          <Card.Content>
            <Card.Header>Forecast Dates</Card.Header>
            <Form.Field inline>
              <label>First</label>
              <Input
                type="numeric"
                value={this.state.dates2[0].year}
                onChange={(e, d) => this.onDateUpdate(d.value, 'year', 0)}
              />
              <Select
                options={QUARTER_OPTIONS}
                value={this.state.dates2[0].quarter}
                onChange={(e, d) => this.onDateUpdate(d.value, 'quarter', 0)}
              />
            </Form.Field>
            <Form.Field inline>
              <label>Last</label>
              <Input
                value={this.state.dates2[1].year}
                onChange={(e, d) => this.onDateUpdate(d.value, 'year', 1)}
              />
              <Select
                options={QUARTER_OPTIONS}
                value={this.state.dates2[1].quarter}
                onChange={(e, d) => this.onDateUpdate(d.value, 'quarter', 1)}
              />
            </Form.Field>
          </Card.Content>
        </Form>
        <Card fluid>
          <Card.Content>
            <Card.Header>
              <Icon name="upload" />
              Main Data File
            </Card.Header>
            <FileDropzone onDrop={this.onDropRaw} />
          </Card.Content>
        </Card>
        <Card fluid>
          <Card.Content>
            <Card.Header>
              <Icon name="upload" />
              Auxiliary Data File
            </Card.Header>
            <FileDropzone onDrop={this.onDropAux} />
          </Card.Content>
        </Card>
        <Button onClick={this.onSubmit}>Okay</Button>
      </div>
    );
  }
}
