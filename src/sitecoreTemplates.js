const stripNewLine = (text) => {
  var isTicked = false;
  if (text.slice(-2) == '\n') {
    text = text.slice(0, -2);
    isTicked = true;
  }
  if (text.slice(0, 2) == '\n') {
    text = text.slice(2);
    isTicked = true;
  }

  if (!isTicked) {
    return text;
  } else {
    return stripNewLine(text);
  }
};

const formatBodyText = (text) => {
  text = stripNewLine(text);
  text = text.split('\n').join('<br/><br/>');
  return text;
};

export const economicBearingsTemplate = (title, subtitle, body, bodyIsRaw) => {
  var bodyText, bottomLine;
  if (!bodyIsRaw) {
    if (
      body
        .toLowerCase()
        .split('\n')
        .includes('bottom line')
    ) {
      var bodyOld = body.split('\n');
      var idx = body
        .toLowerCase()
        .split('\n')
        .indexOf('bottom line');
      bottomLine = bodyOld.slice(idx + 1).join('\n');
      body = bodyOld.slice(0, idx).join('\n');
    }

    body = formatBodyText(body);

    bodyText = `${body}<br/><br/>`;
    if (bottomLine) {
      bottomLine = formatBodyText(bottomLine);
      bodyText += `<h5>Bottom Line</h5>${bottomLine}<br/><br/>`;
    }
  } else {
    bodyText = body;
  }

  return `
<!-- ${title} -->
<!-- ECONOMIC BEARINGS TEMPLATE -->
<!-- 

DS Subscribe link (insert directly):
/Home/library/articles/advisory/2018/economic-analysis/misc/ds-subscribe.aspx 

-->
<style>
.article__content {
margin-top:-40px;
}

.byline {
float:right;
margin:20px 10px 40px 40px;
min-width:300px;
}

@media screen and (max-width: 589px) {
.byline {
margin: 20px 10px 70px 10px;
min-width: 370px;
float: none;
}}

</style>

<div class="byline"><!--<strong>About the author</strong><br />-->
 <a href="/?sc_itemid=%7BCA082F46-0E94-48A4-AFB5-EBC2E8F317D4%7D"><strong><img alt="Diane Swonk" src="-/media/65FF47EB67BB46D2925FFF4EB1EBB327.ashx?h=106&amp;w=125" style="height: 106px; width: 125px; float: left; margin-right: 15px;">
  Diane Swonk</strong></a><br>
  Chief economist<br>
<a rel="noopener noreferrer" href="https://twitter.com/dianeswonk" target="_blank">@DianeSwonk</a>
 
</div>
${bodyText}
<div>
  <strong>Media Contact</strong><br />
  Karen Nye<br />
  <strong>T</strong> +1 312 602 8973<br/>
  <a href="mailto:Karen.Nye@us.gt.com">Karen.Nye@us.gt.com</a><br/>
  <strong>Other Inquiries</strong><br />
  Na Tasha Lowe<br />
  <strong>T</strong> +1 312 754 7368<br/>
  <a href="mailto:NaTasha.Lowe@us.gt.com">NaTasha.Lowe@us.gt.com</a><br/>
</div>
<div>
<br/>
  <a href="/?sc_itemid=%7BFFA4BF7B-4D32-444C-8799-875CC3D5BF23%7D"><strong>More from Diane Swonk</strong></a><br />
  <br />
  <br />
</div>
<br/>
<div id="legal-disclaimer"><p style="font-size: 0.9em;font-weight:100;line-height: 1.1em;text-align: justify;"><span>
  Copyright &copy; ${new Date().getFullYear()} Diane Swonk – All rights reserved.&nbsp; The information provided herein is believed to be obtained from sources deemed to be accurate, timely and reliable. However, no assurance is given in that respect. The reader should not rely on this information in making economic, financial, investment or any other decisions. This communication does not constitute an offer or solicitation, or solicitation of any offer to buy or sell any security, investment or other product. Likewise, this communication serves to provide certain opinions on current market conditions, economic policy or trends and is not a recommendation to engage in, or refrain from engaging, in a particular course of action.
    </span></p>
</div>
  `;
};

export const economicCurrentsTemplate = (
  title,
  subtitle,
  body,
  bodyIsRaw,
  imageLink,
  pdfLink,
  month,
) => {
  var bodyText = '';
  if (!bodyIsRaw) {
    if (imageLink) {
      bodyText += `<span class="pullquote-right" style="width:340px;margin-top:0px;padding-right:0px;margin-right:0px;margin-bottom:0px;font-weight:bold;font-style:italic;">
    <img alt="Accompanying Image" src="${imageLink}" target="_blank" />
</span>`;
    }

    body = formatBodyText(body);

    bodyText += `
${body}
<br />
<br />`;

    if (pdfLink) {
      bodyText += `
<strong><a href="${pdfLink}">Read the full 
${month} 
Economic Currents</a></strong>
<br />
<br />`;
    }
  } else {
    bodyText = body;
  }

  return `
<!-- ${title} -->
<!-- ECONOMIC CURRENTS TEMPLATE -->
<!-- 

DS Subscribe link (insert directly):
/Home/library/articles/advisory/2018/economic-analysis/misc/ds-subscribe.aspx 

-->
${bodyText}
<div>
    <strong>About the author</strong><br />
    <a href="~/link.aspx?_id=CA082F460E9448A4AFB5EBC2E8F317D4&amp;_z=z"><strong><img alt="Diane Swonk" src="-/media/65FF47EB67BB46D2925FFF4EB1EBB327.ashx?h=106&amp;w=125" style="height: 106px; width: 125px; float: left; margin-right: 15px;" />
        </strong>Diane Swonk</a><br />
    Chief economist<br />
    <a rel="noopener noreferrer" href="https://twitter.com/dianeswonk" target="_blank">Twitter: @DianeSwonk</a>
    <br />
    <br />
    <br />
    <strong>Media Contact</strong><br />
    Karen Nye<br />
    <strong>T</strong> +1 312 602 8973<br />
    <a href="mailto:Karen.Nye@us.gt.com">Karen.Nye@us.gt.com</a><br />
    <strong>Other Inquiries</strong><br />
    Na Tasha Lowe<br />
    <strong>T</strong> +1 312 754 7368<br />
    <a href="mailto:NaTasha.Lowe@us.gt.com">NaTasha.Lowe@us.gt.com</a><br />
</div>
<div>
    <br />
    <a href="~/link.aspx?_id=FFA4BF7B4D32444C8799875CC3D5BF23&amp;_z=z"><strong>More from Diane Swonk</strong></a><br />
    <br />
    <br />
</div>
<br />
<div id="legal-disclaimer">
    <p style="font-size: 0.9em;font-weight:100;line-height: 1.1em;text-align: justify;"><span>
      <!-- 5. Check to ensure that copyright date is correct -->
            Copyright &copy; ${new Date().getFullYear()} Diane Swonk – All rights reserved.&nbsp; The information provided herein is believed to be obtained from sources deemed to be accurate, timely and reliable. However, no assurance is given in that respect. The reader should not rely on this information in making economic, financial, investment or any other decisions. This communication does not constitute an offer or solicitation, or solicitation of any offer to buy or sell any security, investment or other product. Likewise, this communication serves to provide certain opinions on current market conditions, economic policy or trends and is not a recommendation to engage in, or refrain from engaging, in a particular course of action.
        </span></p>
</div>
`;
};
